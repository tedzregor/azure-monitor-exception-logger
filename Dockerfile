FROM node:boron

MAINTAINER Reekoh

COPY . /home/node/azure-monitor-exception-logger

WORKDIR /home/node/azure-monitor-exception-logger

RUN npm i --production

CMD ["node", "app.js"]
