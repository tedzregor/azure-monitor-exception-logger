'use strict'
/* eslint-disable new-cap */

const reekoh = require('reekoh')
const plugin = new reekoh.plugins.ExceptionLogger()
const rkhLogger = new reekoh.logger('azure-monitor-exception-logger')

const BPromise = require('bluebird')
const safeParse = BPromise.method(JSON.parse)

const crypto = require('crypto')

const moment = require('moment')

const get = require('lodash.get')
const uuid = require('uuid/v4')

const rp = require('request-promise')

let url
let jsonData

plugin.on('exception', (logs) => {
  let processingDate = new Date().toUTCString()

  jsonData.ExceptionID = uuid()
  jsonData.Timestamp = processingDate
  jsonData.Code = get(logs, 'name') || logs
  jsonData.ShortMessage = get(logs, 'message') || logs
  jsonData.LongMessage = JSON.stringify(logs, ['message', 'arguments', 'type', 'name'])
  jsonData.Timestamp = get(logs, plugin.config.timestampKey) || moment.utc(processingDate).format('DD/MM/YYYY hh:mm:ss A')

  let body = JSON.stringify(jsonData)
  let contentLength = Buffer.byteLength(body, 'utf8')
  let stringToSign = 'POST\n' + contentLength + '\napplication/json\nx-ms-date:' + processingDate + '\n/api/logs'
  let signature = crypto.createHmac('sha256', new Buffer.from(plugin.config.sharedKey, 'base64')).update(stringToSign, 'utf-8').digest('base64')
  let authorization = 'SharedKey ' + plugin.config.customerID + ':' + signature

  let headers = {
    'content-type': 'application/json',
    'Authorization': authorization,
    'Log-Type': plugin.config.logType,
    'x-ms-date': processingDate
  }

  let options = {
    method: 'POST',
    uri: url,
    headers: headers,
    body: body,
    resolveWithFullResponse: true
  }
  rp(options).then((response) => {
    if (response.statusCode === 200) {
      console.log(jsonData)
      plugin.emit('data.ok')
      plugin.log({
        title: 'Data Received',
        data: jsonData
      })
    } else {
      plugin.logException(new Error(JSON.stringify(response)))
    }
  }).catch((error) => {
    plugin.logException(error)
  })
})

plugin.once('ready', () => {
  jsonData = {
    ExceptionID: null,
    AccountName: plugin.config.accountName,
    PipelineName: plugin.config.pipelineName,
    pluginName: plugin.config.pluginName,
    Code: null,
    ShortMessage: null,
    LongMessage: null,
    Timestamp: null,
    CustomDefinition: {}
  }
  if (plugin.config.customDefinition !== '{}' || plugin.config.customDefinition !== '""' || plugin.config.customDefinition !== null || plugin.config.customDefinition !== 0) {
    safeParse(plugin.config.customDefinition).then((parsedCustomDefinition) => {
      jsonData.CustomDefinition = parsedCustomDefinition
    })
  }

  url = `https://${plugin.config.customerID}.ods.opinsights.azure.com/api/logs?api-version=2016-04-01`
  rkhLogger.info('Azure Monitor Exception Logger has been initialized.')
  plugin.log('Azure Monitor Exception Logger has been initialized.')
  plugin.emit('init')
})

module.exports = plugin
