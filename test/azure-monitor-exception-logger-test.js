/* global describe, it, after, before */
'use strict'

const amqp = require('amqplib')

let _app = null
let _channel = null
let _conn = null

describe('Azure Monitor Exception Logger', () => {

  // process.env.PORT = 8081
  process.env.INPUT_PIPE = 'demo.pipe.exception-logger'
  process.env.BROKER = 'amqp://guest:guest@localhost'
  process.env.CONFIG = '{"customerID":"a81f3c1e-0900-4c85-9720-c2d4d474cae6", "sharedKey":"uc/4Db5PNlARvQ3N03jk1mFaiUqw0jqgAQv7m6m2uuanycFDabLk4mdfRDbkVTjr5/jE85Qs28a4qamzPQeogQ==","logType":"ReekohLoggingEvent2", "accountName":"Watergroup", "pipelineName":"Western Water", "pluginName":"HTTP Ingestion GW","timestampKey":"", "customDefinition": {"Key":"Value"}}'
  
  before('init', function () {
    amqp.connect(process.env.BROKER)
    .then((conn) => {
      _conn = conn
      return conn.createChannel()
    }).then((channel) => {
    _channel = channel
  }).catch((err) => {
    console.log(err)
  })
  })

  after('terminate child process', function (done) {
    _conn.close()
    done()
  })

  describe('#start', function () {
    it('should start the app', function (done) {
      this.timeout(8000)
      _app = require('../app')
      _app.once('init', done)
    })
  })

  describe('#exception', function () {
    it('should log data', function (done) {
      this.timeout(15000)
      /*
      let dummyData = {message: 'TEST_DATA from ESRI Service 3',
                      timeStamp: 'October 13, 2014 11:13:00'}
      */
     //let dummyData = new Error('Error Test')
     let dummyData = {
       name:'sample Error name',
       message: 'Invalid URL'
     }
     _channel.sendToQueue('demo.pipe.exception-logger', new Buffer(JSON.stringify({message: dummyData.message, stack: dummyData.stack, name:dummyData.name})))

      //setTimeout(done, 5000)
      _app.once('data.ok', done)
    })
  })
})

